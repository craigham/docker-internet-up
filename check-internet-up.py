import sys
import time
import os
import paho.mqtt.client as mqtt


try:
    # For Python 3.0 and later
    from urllib.request import urlopen
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen



def checkConnected(url):
    for _ in range(6):
        try:
            html = urlopen(url, timeout=2)
            return 'up'
        except Exception as e:
            time.sleep(10)
            print (e)
    return 'down'

mqtt_url = os.getenv('MQTT_URL', '192.168.1.122')
mqtt_topic = os.getenv('MQTT_TOPIC', 'sensors/internet')

print ('MQTT_URL is :%s' % mqtt_url)
print ('MQTT_TOPIC is :%s' % mqtt_topic)

mqttc = mqtt.Client('internet_status')
mqttc.connect(mqtt_url, 1883)


while True:
    connected = checkConnected('http://www.google.com')
    print(connected)
    mqttc.publish(mqtt_topic, connected)
    time.sleep(60)
